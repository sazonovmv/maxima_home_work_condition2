import java.time.LocalDate;
import java.time.Year;
import java.util.Scanner;
import java.time.temporal.ChronoUnit;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class option2 {

    /*public static int functionCreditLocalDate(int age) {
        LocalDate date = LocalDate.now();
        int yearOfNow = date.getYear();
        date = date.minusYears(age);
        int yearOfBirth = date.getYear();
        int sumCred = 0;
        for (int i = yearOfBirth; i < yearOfNow; ++i) {
            sumCred = sumCred + Year.of(i).length(); // Избавился от проверки на високосный год

        }
        int Month = date.getMonthValue();
        System.out.println(Month);
        if (Month > 2) // если текущая дата позжде февраля
        {
            sumCred = sumCred - 1;
        }
        return sumCred * 2;
    }*/

    public static int functionCreditLocalDateOther(int age) {
        LocalDate date = LocalDate.now();
        int month = date.getMonthValue();
        int day = date.getDayOfMonth();
        LocalDate dateOfBirth = LocalDate.of(date.getYear() - age, month, day);
        long daysBetween = ChronoUnit.DAYS.between(dateOfBirth, date); //Разница между датой рождения и сегоднящним днем
        return (int) daysBetween * 2;
    }

    public static void main(String[] args) {
        System.out.println("Введите ваш имя");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        System.out.println("Введите ваш возраст");
        int age = scanner.nextInt();
        System.out.println("Введите сумму желаемого кредита");
        float creditSum = scanner.nextFloat();
        creditSum = Math.round(creditSum); // Окргуляю сумму кредита в большую или меньшую сторону после занка запятой
        if (!name.equalsIgnoreCase("bob") && age > 18 && creditSum <= age * 100) {
            System.out.println("Кредит выдан");
            System.out.println("Сумма кредита: " + creditSum + " руб. ");
        }
        else if (name.equalsIgnoreCase("bob") && age > 18) {
            //System.out.println("Сумма кредита для Боба: " + functionCreditLocalDate(age) + " рублей");
            System.out.println("Сумма кредита для Боба: " + functionCreditLocalDateOther(age) + " рублей");
        }else {
            System.out.println("Выдача кредита запрещена");
        }
    }

}